**Project Description**

Below is a short description of what the PiHat aims to do in my EEE3088F project. The PiHat that we are building aims to be the brain in a much larger circuit that acts as an Uninterrupted Power Supply (UPS) to a WifiRouter.

**PiHat Instructions**

The PiHat must strictly be used for a WifiRouter that takes an input voltage 12V. The PiHat itself must only take a 3.3V input voltage to power it.

**Input Voltage Switching Regulator PiHat **
-- > Person Responsible: Theodore Psillos (PSLTHE001)

This Regulator ensures that for a voltage that may vary between 5 to 24 V, the output voltage produced will be a constant 3V3 - the specified voltage input for a PiZeroHat. The LTSpice schematic for the submodule was applied to the KiCad program. Having a KiCad schematic allows for the creation of a PCB equivalent diagram. It must be noted that originally we had a linear regulator for this submodule in Assignment 2, however, we changed it to a switching regulator. This was easily done by searching online for a switching regulator that converts a 6V input to 3V3 in LTSpice. This was then used to draw the KiCad schematic and PCB. The Schematic file .sch and PCB will be uploaded to the GitHub repository so that the other team members have access to it. It will also provide the opportunity for cross checking each others schematics and KiCad files.

**LED Array Submodule** 
-- > Person Responsible: Nicholas Rogers (RGRNIC007)

The LED Array provides a visual representation of the supply batteries percentage of fullness to the user. When the supply batteries's voltage drops below specified values the LEDs will start to switch off (100% = Fully Charged, 75%, 50%, 20% and 0%). The LTSpice schematic for the submodule was applied to the KiCad program. Having a KiCad schematic allows for the creation of a PCB equivalent diagram.

**Output Wifi Linear Regulator Submodule** 
-- > Person Responsible: Michael Millard (MLLMIC055)

This Regulator ensures that for a voltage that may vary between 14 to 16 V, the output voltage produced will be a constant 12V - the specified voltage input for a WifiRouter. The LTSpice schematic for the submodule was applied to the KiCad program. Having a KiCad schematic allows for the creation of a PCB equivalent diagram.

The Schematic file .sch and PCB will be uploaded to the GitHub repository so that the other team members have access to it. It will also provide the opportunity for cross checking each others schematics and KiCad files.

**Roles & Contributions**

The team is made up of Michael Millard Theodore Psillos and Nicholas Rogers. We aim to work together and communicate effectively in order to succeed in making a successfuly and working UPS circuit using a PiHat design. I will be in charge of collating all the information together and presenting into a report that is readible. More importantly, this report will ensure that someone with know knowelegde of the topic will be able to understand, to an extent, what the project is trying to achieve. Michael is in charge of the circuits and simulation designs in LTSpice. He has been drawing up the various circuits used to test our specifications and requirements that we listed at the beginning of the project. Finally, Nicholas conducts research into the values that we should be using and any other information relevant to the project. Together we hope that we bring out each other's best skills in ourselves to create an amazing design that is applicable in the real world.
