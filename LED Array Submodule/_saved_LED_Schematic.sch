EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7200 6900 0    50   ~ 0
Nicholas Rogers
Text Notes 7500 7500 0    50   ~ 0
LED Array V1.0
Text Notes 10750 7650 0    50   ~ 0
1
Text Notes 8300 7650 0    50   ~ 0
03/06/21
$Comp
L Device:D_Zener D1
U 1 1 60BAF635
P 2400 2400
F 0 "D1" V 2354 2479 50  0000 L CNN
F 1 "D_Zener" V 2445 2479 50  0000 L CNN
F 2 "Diode_THT:D_5KPW_P7.62mm_Vertical_AnodeUp" H 2400 2400 50  0001 C CNN
F 3 "~" H 2400 2400 50  0001 C CNN
	1    2400 2400
	0    1    1    0   
$EndComp
$Comp
L Device:D_Zener D2
U 1 1 60BB363A
P 3750 2400
F 0 "D2" V 3704 2479 50  0000 L CNN
F 1 "D_Zener" V 3795 2479 50  0000 L CNN
F 2 "Diode_THT:D_5KPW_P7.62mm_Vertical_AnodeUp" H 3750 2400 50  0001 C CNN
F 3 "~" H 3750 2400 50  0001 C CNN
	1    3750 2400
	0    1    1    0   
$EndComp
$Comp
L Device:D_Zener D3
U 1 1 60BB3DF2
P 5050 2400
F 0 "D3" V 5004 2479 50  0000 L CNN
F 1 "D_Zener" V 5095 2479 50  0000 L CNN
F 2 "Diode_THT:D_5KPW_P7.62mm_Vertical_AnodeUp" H 5050 2400 50  0001 C CNN
F 3 "~" H 5050 2400 50  0001 C CNN
	1    5050 2400
	0    1    1    0   
$EndComp
$Comp
L Device:D_Zener D4
U 1 1 60BB451E
P 6250 2400
F 0 "D4" V 6204 2479 50  0000 L CNN
F 1 "D_Zener" V 6295 2479 50  0000 L CNN
F 2 "Diode_THT:D_5KPW_P7.62mm_Vertical_AnodeUp" H 6250 2400 50  0001 C CNN
F 3 "~" H 6250 2400 50  0001 C CNN
	1    6250 2400
	0    1    1    0   
$EndComp
$Comp
L Transistor_BJT:2N2219 Q1
U 1 1 60BB4F9B
P 2900 2850
F 0 "Q1" H 3090 2896 50  0000 L CNN
F 1 "2N2219" H 3090 2805 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-39-3" H 3100 2775 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/2N2219-D.PDF" H 2900 2850 50  0001 L CNN
	1    2900 2850
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:2N2219 Q2
U 1 1 60BB59D8
P 4250 2850
F 0 "Q2" H 4440 2896 50  0000 L CNN
F 1 "2N2219" H 4440 2805 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-39-3" H 4450 2775 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/2N2219-D.PDF" H 4250 2850 50  0001 L CNN
	1    4250 2850
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:2N2219 Q3
U 1 1 60BB5DFD
P 5500 2850
F 0 "Q3" H 5690 2896 50  0000 L CNN
F 1 "2N2219" H 5690 2805 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-39-3" H 5700 2775 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/2N2219-D.PDF" H 5500 2850 50  0001 L CNN
	1    5500 2850
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:2N2219 Q4
U 1 1 60BB65E7
P 6750 2850
F 0 "Q4" H 6940 2896 50  0000 L CNN
F 1 "2N2219" H 6940 2805 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-39-3" H 6950 2775 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/2N2219-D.PDF" H 6750 2850 50  0001 L CNN
	1    6750 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D5
U 1 1 60BB7296
P 3000 1850
F 0 "D5" V 3039 1733 50  0000 R CNN
F 1 "LED" V 2948 1733 50  0000 R CNN
F 2 "Diode_THT:D_5KPW_P7.62mm_Vertical_AnodeUp" H 3000 1850 50  0001 C CNN
F 3 "~" H 3000 1850 50  0001 C CNN
	1    3000 1850
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D6
U 1 1 60BB84AC
P 4350 1850
F 0 "D6" V 4389 1733 50  0000 R CNN
F 1 "LED" V 4298 1733 50  0000 R CNN
F 2 "Diode_THT:D_5KPW_P7.62mm_Vertical_AnodeUp" H 4350 1850 50  0001 C CNN
F 3 "~" H 4350 1850 50  0001 C CNN
	1    4350 1850
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D7
U 1 1 60BB8C66
P 5600 1850
F 0 "D7" V 5639 1733 50  0000 R CNN
F 1 "LED" V 5548 1733 50  0000 R CNN
F 2 "Diode_THT:D_5KPW_P7.62mm_Vertical_AnodeUp" H 5600 1850 50  0001 C CNN
F 3 "~" H 5600 1850 50  0001 C CNN
	1    5600 1850
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D8
U 1 1 60BB95FD
P 6850 1850
F 0 "D8" V 6889 1733 50  0000 R CNN
F 1 "LED" V 6798 1733 50  0000 R CNN
F 2 "Diode_THT:D_5KPW_P7.62mm_Vertical_AnodeUp" H 6850 1850 50  0001 C CNN
F 3 "~" H 6850 1850 50  0001 C CNN
	1    6850 1850
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R1
U 1 1 60BBA0CC
P 1600 1500
F 0 "R1" V 1393 1500 50  0000 C CNN
F 1 "230" V 1484 1500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 1530 1500 50  0001 C CNN
F 3 "~" H 1600 1500 50  0001 C CNN
	1    1600 1500
	0    1    1    0   
$EndComp
Wire Wire Line
	1750 1500 3000 1500
Wire Wire Line
	6850 1500 6850 1700
Wire Wire Line
	6850 2000 6850 2650
Wire Wire Line
	6850 3050 6850 3500
Wire Wire Line
	3000 3500 3000 3050
Wire Wire Line
	4350 3050 4350 3500
Wire Wire Line
	5600 3050 5600 3500
Wire Wire Line
	3000 1500 3000 1700
Connection ~ 3000 1500
Wire Wire Line
	3000 1500 4350 1500
Wire Wire Line
	3000 2000 3000 2650
Wire Wire Line
	4350 1500 4350 1700
Connection ~ 4350 1500
Wire Wire Line
	4350 2000 4350 2650
Wire Wire Line
	5600 1500 5600 1700
Connection ~ 5600 1500
Wire Wire Line
	5600 2000 5600 2650
Wire Wire Line
	5050 2550 5050 2850
Wire Wire Line
	5050 2850 5300 2850
Wire Wire Line
	6250 2550 6250 2850
Wire Wire Line
	6250 2850 6550 2850
Wire Wire Line
	4350 1500 5600 1500
Wire Wire Line
	5600 1500 6850 1500
Wire Wire Line
	6250 2150 6250 2250
Wire Wire Line
	5050 2150 5050 2250
Connection ~ 5050 2150
Wire Wire Line
	5050 2150 6250 2150
Wire Wire Line
	3750 2150 3750 2250
Connection ~ 3750 2150
Wire Wire Line
	3750 2150 5050 2150
Wire Wire Line
	2400 2150 2400 2250
Connection ~ 2400 2150
Wire Wire Line
	2400 2150 3750 2150
Wire Wire Line
	3750 2550 3750 2850
Wire Wire Line
	3750 2850 4050 2850
Wire Wire Line
	2400 2550 2400 2850
Wire Wire Line
	2400 2850 2700 2850
$Comp
L Device:Battery BT1
U 1 1 60BC1A76
P 1100 2800
F 0 "BT1" H 1208 2846 50  0000 L CNN
F 1 "16V" H 1208 2755 50  0000 L CNN
F 2 "Battery:BatteryHolder_Bulgin_BX0036_1xC" V 1100 2860 50  0001 C CNN
F 3 "~" V 1100 2860 50  0001 C CNN
	1    1100 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 1500 1450 1500
Wire Wire Line
	1100 3000 1100 3500
Wire Wire Line
	1100 1500 1100 2150
Wire Wire Line
	1100 2150 2400 2150
Connection ~ 1100 2150
Wire Wire Line
	1100 2150 1100 2600
$Comp
L power:GND #PWR0101
U 1 1 60BCFA36
P 1100 3500
F 0 "#PWR0101" H 1100 3250 50  0001 C CNN
F 1 "GND" H 1105 3327 50  0000 C CNN
F 2 "" H 1100 3500 50  0001 C CNN
F 3 "" H 1100 3500 50  0001 C CNN
	1    1100 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 60BCFDCE
P 3000 3500
F 0 "#PWR0102" H 3000 3250 50  0001 C CNN
F 1 "GND" H 3005 3327 50  0000 C CNN
F 2 "" H 3000 3500 50  0001 C CNN
F 3 "" H 3000 3500 50  0001 C CNN
	1    3000 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 60BD0368
P 4350 3500
F 0 "#PWR0103" H 4350 3250 50  0001 C CNN
F 1 "GND" H 4355 3327 50  0000 C CNN
F 2 "" H 4350 3500 50  0001 C CNN
F 3 "" H 4350 3500 50  0001 C CNN
	1    4350 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 60BD08B4
P 5600 3500
F 0 "#PWR0104" H 5600 3250 50  0001 C CNN
F 1 "GND" H 5605 3327 50  0000 C CNN
F 2 "" H 5600 3500 50  0001 C CNN
F 3 "" H 5600 3500 50  0001 C CNN
	1    5600 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 60BD0DFD
P 6850 3500
F 0 "#PWR0105" H 6850 3250 50  0001 C CNN
F 1 "GND" H 6855 3327 50  0000 C CNN
F 2 "" H 6850 3500 50  0001 C CNN
F 3 "" H 6850 3500 50  0001 C CNN
	1    6850 3500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
